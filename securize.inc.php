<?php
// example
//define('SECURIZE_URL_PARAM', 'pwet');
//define('SECURIZE_URL_PARAM_VALUE', 'pwot');
//define('SECURIZE_USER_AGENT', 'Opera');

// security
if(defined('SECURIZE_URL_PARAM') && defined('SECURIZE_URL_PARAM_VALUE')) {
        // on check que le cookie est là, ou que le useragent est les bon (... pas mieux pour psiandroid...)
        if(!isset($_SESSION)) { 
                session_start(); 
        }
        if(
		(!empty($_SERVER['HTTP_USER_AGENT']) && defined('SECURIZE_USER_AGENT') && $_SERVER['HTTP_USER_AGENT'] === SECURIZE_USER_AGENT) ||
		(!empty($_SESSION[SECURIZE_URL_PARAM]) && $_SESSION[SECURIZE_URL_PARAM] === SECURIZE_URL_PARAM_VALUE)
	) {
                // ok cookie or user agent
        } else {
                // sinon on check l'url
                $value = isset($_POST[SECURIZE_URL_PARAM]) ? $_POST[SECURIZE_URL_PARAM] : isset($_GET[SECURIZE_URL_PARAM]) ? $_GET[SECURIZE_URL_PARAM] : null;
                if($value === SECURIZE_URL_PARAM_VALUE) {
                        // ok !
                        $_SESSION[SECURIZE_URL_PARAM] = $value;
                } else {
                        // bye bye
			unset($_SESSION[SECURIZE_URL_PARAM]);
			//syslog => fail2ban
			error_log(
				$_SERVER['REQUEST_TIME'].
				': unauthorized access blocked by securize.inc.php from IP='
				.(!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']).
				' ROOT='.$_SERVER['DOCUMENT_ROOT'].
				' URI='.$_SERVER['REQUEST_URI'].
				' SCRIPT='.$_SERVER['SCRIPT_NAME']
			);
			// $_SERVER['HTTP_X_FORWARDED_FOR'] in place of $_SERVER['REMOTE_ADDR'] // REMOTE_HOST
                        header("HTTP/1.1 412 Precondition Failed");
                        die(
'<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>412 Precondition Failed</title>
</head><body>
<h1>Precondition Failed</h1>
<p>The requested URL was not correctly called.</p>
</body></html>'
			);
                }
        }
}
